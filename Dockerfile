FROM alpine:3.6
MAINTAINER siamsquared technologies <https://bitbucket.org/SiamSquared_Technologies/mercurial>

RUN apk update && apk add --no-cache --update ca-certificates openssl mercurial && update-ca-certificates && rm /var/cache/apk/*

WORKDIR /root

ENTRYPOINT [ "hg" ]